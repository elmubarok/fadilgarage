<?php include_once("./config.php");

  if(!isset($_SESSION['pref'])){
    header("Location: ".$base_url."/pref.php");
  }
?>

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->

<body>
  <div class="uk-offcanvas-content">
    <!-- Header -->
    <?php include("./components/header.php") ?>
    <!-- /Header -->

    <?php
      $batas = 5;
      $halaman = isset($_GET['halaman']) ? $_GET['halaman'] : 1;

      if (empty($halaman)) {
        $posisi = 0;
        $halaman = 1;
      } else {
        $posisi = ($halaman - 1) * $batas;
      }

      $sql = "SELECT * FROM mobil LIMIT " . $posisi . ", " . $batas;
      $result = $con->query($sql);

      // filtering
      if(isset($_GET['filter'])){
        $price_start = str_replace(".","",$_GET['price_start']);
        $price_end = str_replace(".","",$_GET['price_end']);
        $city = $_GET['city'];
        $brands = isset($_GET['brand']) ? $_GET['brand'] : [];

        $sql = "SELECT * FROM mobil ";
        if($_GET['price_start'] != '' || $_GET['price_end'] != '' || $_GET['city'] != ''){
          $sql .= " WHERE ";
        }else{
          if(isset($_GET['brand'])) $sql .= " WHERE ";
        }
        
        // merk
        if(isset($_GET['brand'])){
          $sql .= "(";
          for($i=0; $i<count($brands); $i++){
            $sql .= " merk='".$brands[$i]."' ";
            $sql .= $i != (count($brands) - 1) ? "OR" : "";
          }
          $sql .= ")";
        }

        // kota
        if($city != ''){
          if($price_start && $price_end || $brands) $sql .= " AND ";
          $sql .= " kota='$city' ";
        }

        // harga
        if($price_start || $price_end){
          if($_GET['city'] != '' || $brands) $sql .= " AND ";
          $sql .= " (harga > " . ($price_start == '' ? '0' : $price_start);

          if($price_end) $sql .= " and harga < " . ($price_end == '' ? '0' : $price_end);
          
          $sql .= ")";
          $sql .= " ORDER BY harga ASC ";
        }

        $sql_paging = $sql;
        $sql .= " LIMIT $posisi, $batas";

        $result = $con->query($sql);
        // echo $sql;

        // echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
      }

      // searching
      if(isset($_GET['search'])){
        $search = $_GET['search'];
        $sql = "SELECT * FROM mobil WHERE merk LIKE '%$search%' OR seri LIKE '%$search%' OR tipe LIKE '%$search%' OR tahun='$search' OR km='$search' OR harga='$search' LIMIT $posisi, $batas";
        $result = $con->query($sql);
      }

    ?>

    <main>
      <section class="uk-section uk-section-small">
        <div class="uk-container">
          <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

            <div class="uk-text-center uk-first-column" style="overflow: hidden; position: relative;">
              <!-- <div class="uk-overlay-primary uk-position-cover" style="left: 30px;"></div> -->
              <!-- <div class="uk-height-large uk-background-cover uk-light uk-flex" uk-parallax="bgy: -200" style="background-image: url('https://images.pexels.com/photos/164634/pexels-photo-164634.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260');">
              </div> -->
              <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="animation: fade; autoplay: true" style="height: 450px;">
                <ul class="uk-slideshow-items">
                  <li style="height: 450px;">
                    <img src="<?= $base_url?>/assets/images/headers/header1.jpeg" alt="" uk-cover>
                  </li>
                  <li style="height: 450px;">
                    <img src="<?= $base_url?>/assets/images/headers/header2.jpeg" alt="" uk-cover>
                  </li>
                  <li style="height: 450px;">
                    <img src="<?= $base_url?>/assets/images/headers/header3.jpeg" alt="" uk-cover>
                  </li>
                  <li style="height: 450px;">
                    <img src="<?= $base_url?>/assets/images/headers/header4.jpeg" alt="" uk-cover>
                  </li>
                  <li style="height: 450px;">
                    <img src="<?= $base_url?>/assets/images/headers/header5.jpeg" alt="" uk-cover>
                  </li>
                </ul>
              </div>
            </div>

            <div class="uk-grid-margin uk-first-column">
              <div class="uk-grid-medium" uk-grid>

                <!-- Sub Sidebar -->
                <?php include("./components/sub-sidebar.php") ?>
                <!-- /Sub Sidebar -->

                <div class="uk-width-expand">
                  <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
                    <div class="uk-first-column">

                      <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                        <div class="uk-grid-collapse uk-child-width-1-1" id="products" uk-grid>

                            <?php if(isset($_GET['search'])):?>
                              <div class="uk-card-header uk-background-primarys">
                                <div class="uk-grid-small uk-flex-middle" uk-grid>
                                  <div class="uk-width-1-1 uk-width-expand@s uk-flex uk-flex-center uk-flex-left@s uk-text-small">
                                    <span class="uk-margin-small-right uk-text-white" style="color: #ffffffssss;">Hasil pencarian "<strong><?= $_GET['search']?></strong>"</span>
                                  </div>
                                  <div class="uk-width-1-1 uk-width-auto@s uk-flex uk-flex-center uk-flex-middle">
                                    <ul class="uk-subnav uk-margin-remove">
                                      <li>
                                        <a class="uk-text-lowercase" href="<?= $base_url?>" uk-icon="history">clear search&nbsp;&nbsp;</a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            <?php endif;?>

                          <div>
                            <div class="uk-grid-collapse uk-child-width-1-3 stm-products-grid sjs-products-grid tm-products-list js-products-list" uk-grid>

                              <?php while ($row = $result->fetch_assoc()) : ?>
                                <article class="tm-product-card">
                                  <div class="tm-product-card-media">
                                    <div class="tm-ratio tm-ratio-4-3">
                                      <a class="tm-media-box" href="product.html">
                                        <figure class="tm-media-box-wrap"><img src="<?= $base_url ?>/assets/images/cars/2_<?= $row['nopol'] ?>.jpg" alt='' /></figure>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="tm-product-card-body">
                                    <div class="tm-product-card-info">
                                      <div class="uk-text-meta uk-margin-xsmall-bottom"><?= $row['merk'] ?></div>
                                      <h3 class="tm-product-card-title">
                                        <a class="uk-link-heading" href="product.html"><?= $row['seri'] ?> - <?= $row['tahun'] ?></a>
                                      </h3>
                                      <ul class="uk-list uk-text-small tm-product-card-properties">
                                        <li><span class="uk-text-muted">Tipe: </span><span><?= $row['tipe'] ?></span></li>
                                        <li><span class="uk-text-muted">CC: </span><span><?= $row['CC'] ?></span></li>
                                        <li><span class="uk-text-muted">Warna: </span><span><?= $row['warna'] ?></span></li>
                                        <li><span class="uk-text-muted">Kota: </span><span><?= $row['kota'] ?></span></li>
                                      </ul>
                                    </div>
                                    <div class="tm-product-card-shop" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
                                      <div class="tm-product-card-prices" style="flex-grow: 0;">
                                        <div class="tm-product-card-price">
                                          <small>Rp <?= str_replace(",", ".", number_format($row['harga'])) ?></small>
                                        </div>
                                      </div>
                                      <div class="tm-product-card-add">
                                        <button onclick="window.open('<?= $base_url ?>/detail.php?id=<?= $row['id_mobil'] ?>', '_self')" class="uk-button uk-button-primary tm-product-card-add-button tm-shine jss-add-to-cart">
                                          <span class="tm-product-card-add-button-icon" uk-icon="cart"></span><span class="tm-product-card-add-button-text">detail</span>
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </article>
                              <?php endwhile; ?>

                            </div>
                          </div>

                          <!-- <div>
                              <button class="uk-button uk-button-default uk-button-large uk-width-1-1" style="border-top-left-radius: 0; border-top-right-radius: 0;">
                                <span class="uk-margin-small-right" uk-icon="icon: plus; ratio: .75;"></span><span>Load more</span>
                              </button>
                            </div> -->

                        </div>
                      </div>

                    </div>

                    <!-- Pagination -->
                    <?php include("./components/pagination.php") ?>
                    <!-- /Pagination -->

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Main Footer -->
      <?php include("./components/sub-main-footer.php") ?>
      <!-- /Main Footer -->

    </main>

    <!-- Footer -->
    <?php include("./components/footer.php") ?>
    <!-- /Footer -->

    <!-- Offcanvas -->
    <?php include("./components/offcanvas.php") ?>
    <!-- /Offcanvas -->

  </div>

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

</body>

</html>