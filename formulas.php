<?php
  include_once("./functions.php");

  $input = [3,3,3,3,3];
  $input_id = [1,2,3,4,5];
  
  $datas = [
    [3,1,2,2,3],
    [2,1,3,3,3],
    [1,3,2,3,1],
    [2,1,2,2,3],
    [1,1,1,3,3]
  ];

  // $input = [3,3,1,2,1];
  // $input_id = [1,2,3];
  
  // $datas = [
  //   [2,1,3,2,1],
  //   [3,2,2,3,3],
  //   [3,1,3,2,3],
  // ];

  $datas_label = [
    "AVANZA", "KIJANG", "AVP", "INNOVA", "XENIA"
  ];

  function calculate($input, $input_id, $datas){

    $datas_x = count($datas[0]);
    $datas_y = count($datas);

    $datas_dimension = $datas_x * $datas_y;

    // transpose
    $datas_tpose = tPose($datas);

    // prettyEcho($datas_tpose);
    
    $sqrt = [];
    
    $datas_t_x = count($datas_tpose[0]);
    $datas_t_y = count($datas_tpose);

    // calculate step 1
    for($j=0; $j < $datas_t_y; $j++){
      $sqrt_temp = 0;

      for($k=0; $k < $datas_t_x; $k++){
        $sqrt_temp += pow( $datas_tpose[$j][$k], 2 );
      }

      $sqrt_temp = sqrt($sqrt_temp);
      array_push($sqrt, $sqrt_temp);
    }

    // echo "Step 1 : <br>";
    // prettyEcho1($sqrt);

    // prettyEcho($datas);

    $step2 = [];

    // calculate step 2
    for($i=0; $i < $datas_y; $i++){
      $step2_temp = [];
      for ($j=0; $j < $datas_x; $j++) { 
        $step2_temp[$j] = ($datas[$i][$j] / $sqrt[$j]);
      }
      array_push($step2, $step2_temp);
    }

    // echo "Step 2 : <br>";
    // prettyEcho($step2);

    $step3 = [];
    
    // calculate step 3
    for($i=0; $i < count($step2); $i++){
      $step3_temp = [];
      for ($j=0; $j < count($step2[$i]); $j++) { 
        $step3_temp[$j] = ($step2[$i][$j] * $input[$j]);
      }
      array_push($step3, $step3_temp);
    }

    // echo "Step 3 : <br>";
    // prettyEcho($step3);

    $max = [];
    $min = [];

    $step3_t = tPose($step3);

    for ($i=0; $i < count($step3_t); $i++) {
      array_push($max, max($step3_t[$i]));
      array_push($min, min($step3_t[$i]));
    }

    // echo "Max : <br>";
    // prettyEcho1($max);
    // echo "Min : <br>";
    // prettyEcho1($min);

    $d_plus = [];
    $d_minus = [];

    // d+ & d-
    // d+ => step3 - max
    // d- => step3 - min
    for($j=0; $j < count($step3); $j++){
      $sqrt_plus_temp = 0;
      $sqrt_min_temp = 0;

      for($k=0; $k < count($step3[$j]); $k++){
        $sqrt_plus_temp += pow( ($max[$k] - $step3[$j][$k]), 2 );
        $sqrt_min_temp += pow( ($min[$k] - $step3[$j][$k]), 2 );
      }

      $sqrt_plus_temp = sqrt($sqrt_plus_temp);
      $sqrt_min_temp = sqrt($sqrt_min_temp);

      array_push($d_plus, $sqrt_plus_temp);
      array_push($d_minus, $sqrt_min_temp);
    }

    // echo "D+ : <br>";
    // prettyEcho1($d_plus);
    // echo "D- : <br>";
    // prettyEcho1($d_minus);

    $v = [];

    // v
    // d- / (d- + d+)
    for ($i=0; $i < count($d_plus); $i++) { 
      array_push(
        $v, 
        ( 
          $d_minus[$i] / ( $d_minus[$i] + $d_plus[$i] ) 
        ) 
      );
    }

    // echo "V : <br>";
    // prettyEcho1($v);


    // TOPSIS

    $topsis_raw_1 = [];
    $topsis_raw_2 = [];

    for ($i=0; $i < $datas_y; $i++) { 
      $current_outer_row = $datas[$i];
      $row_group = [];
      $row_group2 = [];

      for($j=0; $j < $datas_y; $j++){
      
        $topsis_raw_1_temp = [];
        $topsis_raw_2_temp = [];
    
        for($k=0; $k < $datas_x; $k++){
          $topsis_raw_1_temp[$k] = $datas[$j][$k] - $current_outer_row[$k];
          $topsis_raw_2_temp[$k] = $topsis_raw_1_temp[$k] <= 0 ? 0 : 1;
        }
    
        array_push($row_group, $topsis_raw_1_temp);
        array_push($row_group2, $topsis_raw_2_temp);
      }

      array_push($topsis_raw_1, $row_group);
      array_push($topsis_raw_2, $row_group2);

    }

    // echo "Topsis RAW 1 : <br>";
    // prettyEcho2($topsis_raw_1);

    // echo "Topsis RAW 2 : <br>";
    // prettyEcho2($topsis_raw_2);

    // Hasil Non-Topsis

    $result_non_topsis = [];

    // TODO count V
    for ($i=0; $i < count($topsis_raw_2); $i++) {
      $result_non_topsis_temp = [];
      for ($j=0; $j < count($topsis_raw_2[$i]); $j++) {
        $result_non_topsis_temp[$j] = (array_sum($topsis_raw_2[$i][$j]) / count($v));
      }
      array_push($result_non_topsis, $result_non_topsis_temp);
    }

    // echo "Hasil Non Topsis : <br>";
    // prettyEcho($result_non_topsis);

    // Hasil TOPSIS

    $result_topsis = [];

    // print_r($topsis_raw_2[0][1]);
    // $asd = array_map(function($a, $b){ return $a * $b; }, $v, $topsis_raw_2[0][1]);
    // print_r(array_sum($asd));

    for ($i=0; $i < count($topsis_raw_2); $i++) { 
      $result_topsis_temp = [];
      for ($j=0; $j < count($topsis_raw_2[$i]); $j++) {
        $result_topsis_temp[$j] = array_sum(array_map(function($a, $b){ return $a * $b; }, $v, $topsis_raw_2[$i][$j]));
      }
      array_push($result_topsis, $result_topsis_temp);
    }

    // echo "Hasil Topsis : <br>";
    // prettyEcho($result_topsis);

    // Before final result
    
    $result_topsis_t = tPose($result_topsis);

    // echo "Before Final : <br>";
    // prettyEcho($result_topsis_t);

    $lf = [];
    $ef = [];

    // LF Sum Horizontal
    for ($i=0; $i < count($result_topsis_t); $i++) {
      array_push( $lf, (array_sum($result_topsis_t[$i]) / count($result_topsis_t)) );
    }

    // EF Sum Vertical
    for ($i=0; $i < count($result_topsis); $i++) {
      array_push( $ef, (array_sum($result_topsis[$i]) / count($result_topsis)) );
    }

    // echo "LF : <br>";
    // prettyEcho1($lf);

    // echo "EF : <br>";
    // prettyEcho1($ef);

    // NF

    $nf = [];

    for ($i=0; $i < count($result_topsis); $i++) { 
      array_push( $nf, abs(($lf[$i] - $ef[$i])) );
    }

    // echo "NF : <br>";
    // prettyEcho1($nf);

    // sort
    $sorted = $nf;
    rsort($sorted);

    $final = [];

    for ($i=0; $i < count($nf); $i++) {
      $s = array_search($nf[$i], $sorted);
      $s = $s+1;

      $final["$s"] = ["id"=> $input_id[$i], "val"=>$nf[$i]];
    }

    // echo "FINAL : <br>";
    // foreach ($final as $key => $value) {
    //   echo $value." => ".$key;
    //   echo "<br>";
    // }

    // Before final result (NON TOPSIS)
    
    $result_non_topsis_t = tPose($result_non_topsis);

    // echo "Before Final : <br>";
    // prettyEcho($result_non_topsis_t);

    $lf_nt = [];
    $ef_nt = [];

    // LF Sum Horizontal
    for ($i=0; $i < count($result_non_topsis_t); $i++) {
      array_push( $lf_nt, (array_sum($result_non_topsis_t[$i]) / count($result_non_topsis_t)) );
    }

    // EF Sum Vertical
    for ($i=0; $i < count($result_non_topsis); $i++) {
      array_push( $ef_nt, (array_sum($result_non_topsis[$i]) / count($result_non_topsis)) );
    }

    // echo "LF NON Topsis : <br>";
    // prettyEcho1($lf_nt);

    // echo "EF NON Topsis : <br>";
    // prettyEcho1($ef_nt);

    // NF NON Topsis

    $nf_nt = [];

    for ($i=0; $i < count($result_non_topsis); $i++) { 
      array_push( $nf_nt, abs(($lf_nt[$i] - $ef_nt[$i])) );
    }

    // echo "NF NON Topsis : <br>";
    // prettyEcho1($nf_nt);

    // Order by Multiplication

    $n_nf = [];

    for ($i=0; $i < count($v); $i++) { 
      array_push($n_nf, ($v[$i] * $nf_nt[$i]));
    }

    // echo "N_NF : <br>";
    // prettyEcho1($n_nf);

    // Weight

    $w0 = 0;
    $w1 = 0;

    // step 1
    for ($i=0; $i < count($v); $i++) { 
      $w0 += pow( $v[$i], 2 );
      $w1 += pow( $nf_nt[$i], 2 );
    }

    $w0 = sqrt($w0);
    $w1 = sqrt($w1);

    // echo "W0 : <br>"; echo $w0."<br><br>";
    // echo "W1 : <br>"; echo $w1."<br><br>";

    $w0_0 = [];
    $w1_0 = [];

    // step 2
    for ($i=0; $i < count($v); $i++) {
      array_push($w0_0, ($v[$i] / $w0));
      array_push($w1_0, ($nf_nt[$i] / $w1));
    }

    // echo "W0_0 : <br>";
    // prettyEcho1($w0_0);
    // echo "W1_0 : <br>";
    // prettyEcho1($w1_0);

    // step 3
    $w0_0_max = max($w0_0);
    $w1_0_max = max($w1_0);
    $w0_0_min = min($w0_0);
    $w1_0_min = min($w1_0);

    // echo "W0 Max : <br>"; echo $w0_0_max."<br><br>";
    // echo "W1 Max : <br>"; echo $w1_0_max."<br><br>";
    // echo "W0 Min : <br>"; echo $w0_0_min."<br><br>";
    // echo "W1 Min : <br>"; echo $w1_0_min."<br><br>";

    $w_max_sqrt = [];
    $w_min_sqrt = [];

    // step 4
    for ($i=0; $i < count($w0_0); $i++) {
      $w_max_sqrt_temp = 0;
      $w_min_sqrt_temp = 0;
      for ($j=0; $j < 1; $j++) { 
        $w_max_sqrt_temp = pow(($w0_0_max - $w0_0[$i]), 2) + pow(($w1_0_max - $w1_0[$i]), 2);
        $w_min_sqrt_temp = pow(($w0_0_min - $w0_0[$i]), 2) + pow(($w1_0_min - $w1_0[$i]), 2);
      }
      array_push($w_max_sqrt, sqrt($w_max_sqrt_temp));
      array_push($w_min_sqrt, sqrt($w_min_sqrt_temp));
    }

    // echo "W_MAX_SQRT : <br>";
    // prettyEcho1($w_max_sqrt);
    // echo "W_MIN_SQRT : <br>";
    // prettyEcho1($w_min_sqrt);

    $n_nf_final = [];

    for ($i=0; $i < count($w_max_sqrt); $i++) { 
      array_push(
        $n_nf_final,
        ($w_min_sqrt[$i] / ($w_min_sqrt[$i] + $w_max_sqrt[$i]))
      );
    }

    // echo "N_NF FINAL : <br>";
    // prettyEcho1($n_nf_final);

    // sort
    $sorted_nt = $n_nf_final;
    rsort($sorted_nt);

    $final_nt = [];

    for ($i=0; $i < count($n_nf_final); $i++) {
      $s = array_search($n_nf_final[$i], $sorted_nt);
      $s = $s+1;

      $final_nt["$s"] = ["id"=> $input_id[$i], "val"=>$n_nf_final[$i]];
    }

    // echo "FINAL : <br>";
    // foreach ($final_nt as $key => $value) {
    //   echo $value["id"]." => ".$value["val"]." : [".$key."]";
    //   echo "<br>";
    // }

    // return $final;
    return $final_nt;
  }

  // calculate($input, $input_id, $datas);
  // echo "Final: <br>";
  // print_r(calculate($input, $input_id, $datas));