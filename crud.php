<?php
  include_once("./config.php");

  if(!isset($_SESSION["role"])){
    header("Location: ".$base_url);
  }

  $data = null;

  if (isset($_POST['t11'])) {
    $nopol = $_POST['t11'];
    $merk = $_POST['t12'];
    $seri = $_POST['t13'];
    $tipe = $_POST['t14'];
    $size = $_POST['t15'];
    $warna = $_POST['t115'];
    $tahun = $_POST['t16'];
    $km = $_POST['t17'];
    $kota = $_POST['t172'];
    $harga = $_POST['t18'];
    $ket = $_POST['t19'];
    $informer = $_POST['t20'];

    $transmisi = $_POST['t21'];
    $bahan_bakar = $_POST['t22'];
    
    if(!isset($_POST['submit_edit'])){
      // insert new data
      $sql = "INSERT INTO `mobil` (`nopol`, `merk`, `seri`, `tipe`, `CC`, `transmisi`, `bahan_bakar`, `warna`, `tahun`, `km`, `kota`, `harga`, `ket`, `informer`, `st_nilai`) VALUES ('$nopol', '$merk', '$seri', '$tipe', '$size', '$warna', '$tahun', '$km', '$kota', '$harga', '$ket', '$informer', 'Belum')";
    }else{
      $id = $_GET['edit'];
      // edit data
      $sql = "UPDATE `mobil` SET  `nopol` = '$nopol',`merk` = '$merk', `seri` = '$seri',`tipe` = '$tipe',`CC` = '$size',`transmisi` = '$transmisi',`bahan_bakar` = '$bahan_bakar',`warna` = '$warna',`tahun` = '$tahun',`km` = '$km',`kota` = '$kota', `harga` = '$harga',`ket` = '$ket',`informer` = '$informer' WHERE `id_mobil` = '$id' ";
    }

    $jalan = $con->query($sql);

    if(!isset($_POST['submit_edit'])){
      // only insert photo
      // not update photo
      move_uploaded_file($_FILES["f1"]["tmp_name"], "assets/images/cars/1_$nopol.jpg");
      move_uploaded_file($_FILES["f2"]["tmp_name"], "assets/images/cars/2_$nopol.jpg");
      move_uploaded_file($_FILES["f3"]["tmp_name"], "assets/images/cars/3_$nopol.jpg");
      move_uploaded_file($_FILES["f4"]["tmp_name"], "assets/images/cars/4_$nopol.jpg");
    }

    header("Location: ".$base_url."/admin.php");
  }

  if(isset($_GET['edit'])){
    // $_GET['edit'] value is id_mobil
    $q = $con->query("SELECT * FROM mobil WHERE id_mobil = ".$_GET['edit']);
    $data = $q->fetch_assoc();
  }
?>

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->

<body>
  <div class="uk-offcanvas-content">

    <?php
    $userid = $_SESSION['userid'];
    $sql = "SELECT * FROM mobil WHERE informer = '$userid'";
    $result = $con->query($sql);
    ?>

    <main>
      <section class="uk-section uk-section-small">
        <div class="uk-container">

          <h3><?= isset($_GET['edit']) ? 'Ubah' : 'Tambah'?> Data.</h3>

          <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

            <!-- User Info -->
            <?php include("./components/user-info.php")?>
            <!-- User Info -->

            <div class="uk-grid-margin uk-first-column uk-width-1-1 uk-width-expand@m">
              <div class="uk-grid-medium" uk-grid>

                <div class="uk-width-expand">
                  <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

                    <div class="uk-first-column">

                      <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                        <div class="uk-grid-collapse uk-child-width-1-1" id="products" uk-grid>

                          <div class="uk-card-header">
                            <div class="uk-grid-small uk-flex-middle" uk-grid>
                              <div class="uk-width-1-1 uk-width-expand@s uk-flex uk-flex-center uk-flex-left@s uk-text-small">
                              </div>
                              <div class="uk-width-1-1 uk-width-auto@s uk-flex uk-flex-center uk-flex-middle">
                                <ul class="uk-subnav uk-margin-remove">
                                  <li>
                                    <button type="submit" form="frmTambah" class="uk-button uk-button-default">simpan</button>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>

                          <div>
                            <div class="uk-grid-collapse uk-child-width-1-1 uk-padding" uk-grid>

                              <form class="uk-form-horizontal" id="frmTambah" action="" method="post" enctype="multipart/form-data">

                                <!-- userid -->
                                <input type="hidden" name="t20" value="<?= $_SESSION['userid'] ?>">
                                <!-- nopol -->
                                <input type="hidden" name="t11" value="<?= $data ? $data['nopol'] : uniqid() ?>">
                                <!-- km -->
                                <input type="hidden" name="t17" value="0">

                                <!-- for edit ability -->
                                <?php if(isset($_GET['edit'])):?>
                                <input type="hidden" name="submit_edit" value="">
                                <?php endif;?>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Merk</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t12" type="text" placeholder="..." value="<?= $data ? $data['merk'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Seri</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t13" type="text" placeholder="..." value="<?= $data ? $data['seri'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Tipe</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t14" type="text" placeholder="..." value="<?= $data ? $data['tipe'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">CC</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t15" type="text" placeholder="..." value="<?= $data ? $data['CC'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Transmisi</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t21" type="text" placeholder="..." value="<?= $data ? $data['transmisi'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Bahan Bakar</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t22" type="text" placeholder="..." value="<?= $data ? $data['bahan_bakar'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Warna</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t115" type="text" placeholder="..." value="<?= $data ? $data['warna'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Tahun</label>
                                  <div class="uk-form-controls">
                                    <input required maxlength="4" class="uk-input" name="t16" type="text" placeholder="..." value="<?= $data ? $data['tahun'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Kota</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t172" type="text" placeholder="..." value="<?= $data ? $data['kota'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Harga</label>
                                  <div class="uk-form-controls">
                                    <input required class="uk-input" name="t18" type="text" placeholder="..." value="<?= $data ? $data['harga'] : '' ?>">
                                  </div>
                                </div>

                                <div class="uk-margin-small">
                                  <label class="uk-form-label">Keterangan</label>
                                  <div class="uk-form-controls">
                                    <textarea maxlength="200" required class="uk-input" name="t19" style="min-width: 100%; max-width: 100%; min-height: 100px; line-height: 1.5; padding: 8px;"><?= $data ? $data['ket'] : '' ?></textarea>
                                  </div>
                                </div>

                                <?php if(!isset($_GET['edit'])):?>
                                
                                <hr>

                                <div class="uk-child-width-1-4" uk-grid>
                                  <div class="">
                                    <div uk-form-custom="target: true">
                                      <input required type="file" name="f3">
                                      <input class="uk-input uk-form-width-medium" type="text" placeholder="Foto Depan" disabled>
                                    </div>
                                  </div>

                                  <div class="">
                                    <div uk-form-custom="target: true">
                                      <input required type="file" name="f4">
                                      <input class="uk-input uk-form-width-medium" type="text" placeholder="Foto Belakang" disabled>
                                    </div>
                                  </div>

                                  <div class="">
                                    <div uk-form-custom="target: true">
                                      <input required type="file" name="f2">
                                      <input class="uk-input uk-form-width-medium" type="text" placeholder="Foto Kanan" disabled>
                                    </div>
                                  </div>

                                  <div class="">
                                    <div uk-form-custom="target: true">
                                      <input required type="file" name="f1">
                                      <input class="uk-input uk-form-width-medium" type="text" placeholder="Foto Kiri" disabled>
                                    </div>
                                  </div>
                                </div>
                                <?php endif;?>

                              </form>

                            </div>
                          </div>

                        </div>
                      </div>

                    </div>

                  </div>
                </div>

              </div>
            </div>

          </div>

        </div>
      </section>

    </main>

    <!-- Footer -->
    <?php include("./components/footer.php") ?>
    <!-- /Footer -->

    <!-- Offcanvas -->
    <?php include("./components/offcanvas.php") ?>
    <!-- /Offcanvas -->

  </div>

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

</body>

</html>