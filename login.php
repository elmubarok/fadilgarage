<?php include_once("./config.php");

  $failed = false;

  if(isset($_SESSION["role"])){
    $role = $_SESSION["role"];

    header("Location: ".$base_url."/admin.php");
  }

  if(isset($_POST['username'])){
    $uname = $_POST["username"];
    $pass = $_POST["password"];

    // 1 - check penjual
    $q = $con->query("SELECT * FROM informer WHERE 
      user = '$uname' AND pass = '$pass'");

    // check returned rows
    if(mysqli_num_rows($q) > 0){
      $failed = false;
      $data = $q->fetch_assoc();
      // set session
      $_SESSION['userid'] = $data['user'];
      $_SESSION['email'] = $data['email'];
      $_SESSION['role'] = "penjual";
    }else{
      // check admin
      $q = $con->query("SELECT * FROM admin WHERE 
        user = '$uname' AND pass = '$pass'");
      
      if(mysqli_num_rows($q) > 0){
        $failed = false;
        $data = $q->fetch_assoc();
        // set session
        $_SESSION['userid'] = $data['user'];
        $_SESSION['email'] = $data['alamat'];
        $_SESSION['role'] = "admin";
      }else{
        $failed = true;
      }
      
    }

    header("Location: ".$base_url."/login.php");

  }

?>

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->


<body>
  <div class="">

    <main>
      <section class="uk-section uk-section-small">

        <div class="uk-container uk-flex uk-flex-center uk-flex-middle uk-flex-column">
          
          <img src="<?= $base_url ?>/assets/images/logo-2.png" class="uk-margin-medium">

          <div class="uk-child-width-1-1s uk-child-width-1-4@ms uk-margin-small">

            <form action="" class="uk-form-stacked" method="post">
              <div class="uk-margin-small">
                <label class="uk-form-label">Username</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: user"></span>
                  <input autofocus class="uk-input" name="username" type="text" placeholder="...">
                </div>
              </div>

              <div class="uk-margin">
                <label class="uk-form-label">Password</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: unlock"></span>
                  <input class="uk-input" name="password" type="password" placeholder="...">
                </div>
              </div>
              
              
              <div class="uk-margin">
                <button class="uk-button uk-button-primary uk-width-1-1" 
                type="submit">
                  Login
                </button>
              </div>
              <div class="uk-margin">
                <a href="<?= $base_url?>/daftar.php" class="uk-button uk-button-default uk-width-1-1" >
                  Daftar
                </a>
              </div>
            </form>
            
          </div>
          
          <?php if($failed):?>
          <span class="uk-text-danger uk-text-small">
            Username / Password salah.
          </span>
          <?php endif;?>

        </div>
      </section>

    </main>

  </div>

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

</body>

</html>