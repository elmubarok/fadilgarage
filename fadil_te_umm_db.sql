-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2020 at 08:15 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fadil_te_umm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `user`, `pass`, `nama`, `alamat`, `telp`) VALUES
(1, 'a', 'a', 'a', 'a', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `informer`
--

CREATE TABLE `informer` (
  `id_informer` int(11) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `ym` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telp` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `informer`
--

INSERT INTO `informer` (`id_informer`, `user`, `pass`, `email`, `ym`, `alamat`, `telp`) VALUES
(1, 'x', 'x', 'aniesalfian@gmail.com', 'x', 'x', 'x'),
(2, 'anis', 'anis', 'anis', 'anis', 'jl merbabu', '0341215441'),
(3, 'aa', 'aa', 'aa', 'aa', 'aa', 'aa'),
(4, 'bb', 'bb', 'bb@gmail.com', 'bb', 'Surabaya', '098848329');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(11) NOT NULL,
  `id_mobil` varchar(30) NOT NULL,
  `komen` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_mobil`, `komen`) VALUES
(1, '1', 'Percobaan'),
(2, '1', 'Harganya Bisa dikurangi lagi nggak????????'),
(3, '1', 'Harganya Bisa dikurangi lagi nggak????????'),
(4, '1', 'Nggak Bisa bos tu dah miring banget................'),
(5, '2', 'Mas Anis bisa ketemuan nggak untuk membicarakan penjualan mobil itu,'),
(6, '1', 'OPOPJUIO'),
(7, '1', 'IOP'),
(8, '1', 'IOPIOP'),
(9, '1', 'sangat bermanfaat');

-- --------------------------------------------------------

--
-- Table structure for table `mobil`
--

CREATE TABLE `mobil` (
  `id_mobil` int(11) NOT NULL,
  `nopol` varchar(10) NOT NULL,
  `merk` varchar(30) NOT NULL,
  `seri` varchar(30) NOT NULL,
  `tipe` varchar(30) NOT NULL,
  `CC` int(10) DEFAULT NULL,
  `transmisi` varchar(200) DEFAULT NULL,
  `bahan_bakar` varchar(200) DEFAULT NULL,
  `warna` varchar(20) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `km` varchar(10) DEFAULT NULL,
  `kota` varchar(25) NOT NULL,
  `harga` double DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `informer` varchar(30) NOT NULL,
  `st_nilai` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mobil`
--

INSERT INTO `mobil` (`id_mobil`, `nopol`, `merk`, `seri`, `tipe`, `CC`, `transmisi`, `bahan_bakar`, `warna`, `tahun`, `km`, `kota`, `harga`, `ket`, `informer`, `st_nilai`) VALUES
(1, 'N0001', 'Suzuki', 'All New Ertiga', 'GL', 1500, NULL, NULL, 'Hitam', '2018', '0', 'Malang', 224000000, 'Mobil Baru', 'aa', 'Sudah'),
(2, 'N0002AB', 'Suzuki', 'New Baleno', 'Generasi IV', 1370, NULL, NULL, 'Putih', '2019', '30', 'Surabaya', 245000000, 'Pembelian di dealer Kota Surabaya mendapat cashback', 'a', 'Sudah'),
(3, 'L1505BC', 'Suzuki', 'Ignis', 'GX-AGS', 1197, NULL, NULL, 'Orange', '2019', '19', 'Sidoarjo', 200000000, 'Transmisi MT dan AT ready', 'a', 'Sudah'),
(5, 'DK1205BB', 'Toyota', 'New Avanza', '1.5 G', 1500, NULL, NULL, 'Putih', '2020', '26', 'Pasuruan', 221250000, 'Free Service Bulanan', 'a', 'Sudah'),
(6, 'B1778AD', 'Toyota', 'Innova Reborn', '2.0 Q', 2000, NULL, NULL, 'Abu-Abu', '2018', '20', 'JakartaTimur', 414750000, 'Jok Kulit, Window All Auto', 'a', 'Sudah'),
(7, 'AE1509LL', 'Toyota', 'All New Camry', '2.5 V', 2500, NULL, NULL, 'Putih', '2019', '15', 'Kediri', 641850000, 'Sedan Interior Mewah', 'a', 'Sudah'),
(8, 'N1205AA', 'Toyota', 'Fortuner Generasi II', '2.4 G', 2400, NULL, NULL, 'Putih', '2019', '35', 'Malang', 510950000, 'SUV Keren dan Nyaman', 'a', 'Sudah'),
(9, 'DR7338BJ', 'Honda', 'Brio Generasi II', 'E', 1200, NULL, NULL, 'Kuning', '2016', '29', 'Denpasar', 170100000, 'Mobil Nyaman Cocok Untuk Anak Muda', 'a', 'Sudah'),
(10, 'N1444BC', 'Honda', 'Jazz Generasi III', 'S', 1500, NULL, NULL, 'Orange', '2018', '39', 'Malang', 250200000, 'Stylish Model Terbaru', 'a', 'Sudah'),
(11, 'AB1244AC', 'Honda', 'Mobilio Facelift', 'E', 1500, NULL, NULL, '1500', '2018', '22', 'Semarang', 224400000, 'Family Car Paling Laris', 'a', 'Sudah'),
(12, 'AG1245AL', 'Mitsubishi', 'Outlander Sport', 'GLX', 2000, NULL, NULL, 'Emas', '2018', '44', 'Manado', 338000000, 'CrossOver Tercanggih dikelasnya', 'a', 'Belum'),
(13, 'B1205SFI', 'Mitsubishi', 'Pajero Sport Generasi III', 'Dakar(4x4)', 2400, NULL, NULL, 'Putih', '2019', '55', 'Jakarta', 702000000, 'SUV Tangguh Bertenaga', 'a', 'Belum'),
(14, 'B3323LA', 'Mitsubishi', 'Xpander Cross', 'Cross AT', 1500, NULL, NULL, 'Red', '2020', '25000', 'Semarang', 282700000, 'Mulus Full Interior Exterior', 'a', 'Belum'),
(15, 'AA4566EA', 'Nissan', 'All New Serena', 'Highway Star', 2000, NULL, NULL, 'Red Black', '2019', '12000', 'Sidoarjo', 488500000, 'MPV Mewah Sekelas Alphard', 'a', 'Belum'),
(16, 'DR1222BB', 'Nissan', 'Terra', 'VL AT(4x4)', 2500, 'Manual', 'Solar - Bio Solar', 'Earth Brown', '2019', '0', 'Samarinda', 680800000, 'SUV Rally Off Road 4x4 Tangguh', 'a', 'Belum');

-- --------------------------------------------------------

--
-- Table structure for table `mobil_temp`
--

CREATE TABLE `mobil_temp` (
  `id_mobil` varchar(5) NOT NULL,
  `nopol` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mobil_temp`
--

INSERT INTO `mobil_temp` (`id_mobil`, `nopol`) VALUES
('1', 'N0001'),
('2', 'N0002AB'),
('3', 'L1505BC');

-- --------------------------------------------------------

--
-- Table structure for table `pemberli`
--

CREATE TABLE `pemberli` (
  `id_pembeli` int(11) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `telp` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `penilaian`
--

CREATE TABLE `penilaian` (
  `id_mobil` int(11) NOT NULL,
  `nopol` varchar(10) DEFAULT NULL,
  `merk` varchar(30) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `desaign` double DEFAULT NULL,
  `mesin` double DEFAULT NULL,
  `aman` double DEFAULT NULL,
  `nyaman` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `penilaian`
--

INSERT INTO `penilaian` (`id_mobil`, `nopol`, `merk`, `harga`, `desaign`, `mesin`, `aman`, `nyaman`) VALUES
(1, 'N2345ML', 'Mitsubisi', 4, 4, 5, 3, 3),
(3, 'N555ML', 'Honda', 3, 3, 4, 2, 3),
(8, 'N111L', 'Honda', 5, 4, 2, 2, 2),
(11, 'N002L', 'malang', 3, 4, 3, 3, 4),
(12, '5212', 'asd', 1, 3, 3, 5, 4),
(2, 'N0002AB', 'Suzuki', 1, 1, 1, 1, 1),
(5, 'DK1205BB', 'Toyota', 1, 1, 1, 1, 1),
(6, 'B1778AD', 'Toyota', 1, 1, 1, 1, 1),
(7, 'AE1509LL', 'Toyota', 1, 1, 1, 1, 1),
(9, 'DR7338BJ', 'Honda', 1, 1, 1, 1, 1),
(10, 'N1444BC', 'Honda', 1, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `informer`
--
ALTER TABLE `informer`
  ADD PRIMARY KEY (`id_informer`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `mobil`
--
ALTER TABLE `mobil`
  ADD PRIMARY KEY (`id_mobil`);

--
-- Indexes for table `pemberli`
--
ALTER TABLE `pemberli`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- Indexes for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id_mobil`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `informer`
--
ALTER TABLE `informer`
  MODIFY `id_informer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mobil`
--
ALTER TABLE `mobil`
  MODIFY `id_mobil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pemberli`
--
ALTER TABLE `pemberli`
  MODIFY `id_pembeli` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
