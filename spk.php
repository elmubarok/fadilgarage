<?php include_once("./config.php"); include_once("./formulas.php")?>
<!-- Pilih Mobil -->
<?php
  if(!isset($_SESSION['pref'])){
    header("Location: ".$base_url."/pref.php");
  }

  if(isset($_GET['choose_car'])){
    $kosongkan = $con->query("TRUNCATE TABLE `mobil_temp`");
    $no = $_GET['t4'] - 1;

    for ($i = 0; $i <= $no; $i++) {
      $a[$i] = isset($_GET["A$i"]) ? $_GET["A$i"] : '';
      $b[$i] = isset($_GET["t1$i"]) ? $_GET["t1$i"] : '';
      $c[$i] = isset($_GET["t2$i"]) ?  $_GET["t2$i"] : '';

      if ($a[$i] == 'on') {
        $query = "INSERT INTO mobil_temp (id_mobil, nopol)  VALUES('$b[$i]','$c[$i]')";
        $sukses = $con->query($query) or die($con->error);
      }

    }

    $url = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://').$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];
    // redirect to current location without any parameters
    header("Location: ".$url);
  }

  if($_POST){
    $pref = $_SESSION['pref'];

    $inputs = [];
    $ids = [];
    $q = "";

    for($i=0; $i<count($_POST['id_mobil']); $i++){
      $temp = [
        $_POST['t1'][$i],
        $_POST['t2'][$i],
        $_POST['t3'][$i],
        $_POST['t4'][$i],
        $_POST['t5'][$i]
      ];
      array_push($inputs, $temp);
      array_push($ids, $_POST['id_mobil'][$i]);
    }

    $final_result = calculate($pref, $ids, $inputs);
    $mobil_id = [];
    $nilai = [];

    // prettyEcho($inputs);
    // prettyEcho1($ids);
    // prettyEcho1($pref);
    // prettyEcho3($final_result);

    foreach ($final_result as $key => $value) {
      $mobil_id[$key] = $value["id"];
      $nilai[$key] = ["id" => $value["id"], "val" => $value["val"]];
    }

    ksort($mobil_id);
    ksort($nilai);
    $sorted_str = implode(",", array_values($mobil_id));

    $q = "SELECT * FROM mobil WHERE id_mobil IN (".$sorted_str.") ORDER BY FIELD(id_mobil,".$sorted_str.")";
    // echo $q;

    $result_data = $con->query($q);

    foreach ($nilai as $key => $value) {
      $nilai[$value['id']] = $value['val'];
    }
  }

  $preff = $_SESSION['pref'];
?>
<!-- /Pilih Mobil -->

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->

<body>
  <div class="uk-offcanvas-content">
    <!-- Header -->
    <?php include("./components/header.php") ?>
    <!-- /Header -->

    <main>
      <section class="uk-section uk-section-small">
        <div class="uk-container">
          <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

            <?php
              $tampil = "SELECT mobil.*
                          FROM mobil_temp, mobil
                          WHERE mobil_temp.id_mobil = mobil.id_mobil";
              $hasil = $con->query($tampil);
            ?>

            <div class="uk-text-center">
              <h1 class="uk-margin-small-top uk-margin-remove-bottom">
                Rekomendasi Mobil
              </h1>
            </div>

            <div>
              <div class="uk-grid-medium" uk-grid>
                <div class="uk-width-1-1 uk-width-expand">
                  <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                    <div class="uk-card-body">
                      <div class="uk-grid-small">
                        <p>Preferensi Anda</p>
                        <table class="uk-table uk-table-small uk-margin-remove">
                          <tr>
                            <th>HARGA</th>
                            <th>DESAIN</th>
                            <th>CC</th>
                            <th>BAHAN BAKAR</th>
                            <th>TRANSMISI</th>
                          </tr>
                          <tr>
                            <td><?= $terms_val[$preff[0]]?></td>
                            <td><?= $terms_val[$preff[1]]?></td>
                            <td><?= $terms_val[$preff[2]]?></td>
                            <td><?= $terms_val[$preff[3]]?></td>
                            <td><?= $terms_val[$preff[4]]?></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Form Penilaian -->
            <form action="" method="post">
              <div>
                <div class="uk-grid-medium" uk-grid>
  
                  <!-- Left side -->
                  <div class="uk-width-1-1 uk-width-expand@m">
                    <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
  
                      <!-- head -->
                      <header class="uk-card-header uk-text-uppercase uk-text-muted uk-text-right uk-text-small uk-visible@m">
                        <a class="uk-button uk-button-default uk-button-small uk-margin-small" href="#modal-example" uk-toggle>
                          Pilih Mobil
                        </a>
                      </header>
                      <header class="uk-card-header uk-text-uppercase uk-text-muted uk-text-center uk-text-small uk-visible@m">
                        <div class="uk-grid-small uk-child-width-expand@m" uk-grid>
                          <div class="uk-width-1-3 uk-flex uk-flex-center uk-flex-middle">product</div>
                          <div>
                            <div class="uk-grid-small uk-padding-small uk-padding-remove-top">
                              Nilai Tiap Kriteria
                            </div>
                            <div class="uk-grid-small uk-child-width-expand" uk-grid>
                              <div>Harga</div>
                              <div>Desain</div>
                              <div>CC</div>
                              <div>Bahan Bakar</div>
                              <div>Transmisi</div>
                              <div class="uk-width-auto">
                                <div style="width: 0px;"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </header>

                      <?php $k=0; while ($data = $hasil->fetch_assoc()) : //print_r($data)
                      ?>
                        <input type="hidden" value="<?= $data['id_mobil']?>" name="id_mobil[]">

                        <div class="uk-card-body">
                          <div class="uk-grid-small uk-child-width-1-1 uk-child-width-expand@m uk-flex-middle" uk-grid>

                            <!-- Info mobil -->
                            <div class="uk-width-1-3">
                              <div class="uk-grid-small" uk-grid>
                                <div class="uk-width-1-3">
                                  <div class="tm-ratio tm-ratio-4-3">
                                    <a class="tm-media-box" href="">
                                      <figure class="tm-media-box-wrap">
                                        <img src="<?= $base_url ?>/assets/images/cars/1_<?= $data['nopol'] ?>.jpg" alt="">
                                      </figure>
                                    </a>
                                  </div>
                                </div>
                                <div class="uk-width-expand">
                                  <div class="uk-text-meta"><?= $data['merk'] ?></div>
                                  <a class="uk-link-heading" href="">
                                    <?= $data['seri'] ?> - <?= $data['tahun'] ?>
                                    <div class="uk-text-meta">Rp <?= str_replace(",",".", number_format($data['harga']))?></div>
                                  </a> <br>
                                  <small>
                                    Tipe: <strong><?= $data['tipe'] ?></strong> <br>
                                    CC: <strong><?= str_replace(",", ".", number_format($data['CC'])) ?></strong> <br>
                                    Bahan Bakar: <strong><?= $data['bahan_bakar']?></strong> <br>
                                    Transmisi: <strong><?= $data['transmisi']?></strong>
                                  </small>
                                </div>
                              </div>
                            </div>
                            <!-- /Info mobil -->
  
                            <!-- Nilai kriteria -->
                            <div>
                              <div class="uk-grid-small uk-child-width-1-1 uk-child-width-expand@s uk-text-center" uk-grid id="criteria_val">

                                <?php for($i=0; $i<count($terms); $i++):?>
                                <div>
                                  <div class="uk-text-muted uk-hidden@m"><?= $terms[$i]['display']?></div>
                                  <div>
                                    <select name="<?= $terms[$i]['name']?>[]" class="uk-select uk-form-small">
                                      <option <?= isset($inputs) && $inputs[$k][$i] == 3 ? 'selected': ''?> value="3">Cocok</option>
                                      <option <?= isset($inputs) && $inputs[$k][$i] == 2 ? 'selected': ''?> value="2">Ragu - Ragu</option>
                                      <option <?= isset($inputs) && $inputs[$k][$i] == 1 ? 'selected': ''?> value="1">Tidak Cocok</option>
                                    </select>
                                  </div>
                                </div>
                                <?php endfor;?>

                              </div>
                            </div>
                            <!-- /Nilai kriteria -->

                          </div>
                        </div>
                      <?php $k++; endwhile; ?>

                      <div class="uk-margin">
                        <button class="uk-button uk-button-primary uk-width-1-1" style="border-radius: 0;" type="submit">
                          Lihat Rekomendasi
                        </button>
                      </div>
  
                    </div>
  
                    <!-- Hasil rekomendasi -->
                    <div id="recommend_product" class="uk-card uk-card-default uk-card-small tm-ignore-container uk-margin">
                      <header class="uk-card-header uk-text-uppercase uk-text-muted uk-text-left uk-text-small uk-visible@m">
                        <span class="uk-cards-title">Hasil Rekomendasi Kami</span>
                      </header>
  
                      <div class="uk-card-body">

                        <?php if(isset($_POST['id_mobil'])):?>
                        <div>
                          <div class="uk-grid-collapse uk-child-width-1-3 stm-products-grid sjs-products-grid tm-products-list js-products-list" uk-grid>

                            <?php while ($data3 = $result_data->fetch_assoc()):?>
                            <article class="tm-product-card">
                              <div class="tm-product-card-media">
                                <div class="tm-ratio tm-ratio-4-3">
                                  <a class="tm-media-box" href="product.html">
                                    <figure class="tm-media-box-wrap"><img src="<?= $base_url ?>/assets/images/cars/2_<?= $data3['nopol']?>.jpg" alt='' /></figure>
                                  </a>
                                </div>
                              </div>
                              <div class="tm-product-card-body">
                                <div class="tm-product-card-info">
                                  <div class="uk-text-meta uk-margin-xsmall-bottom"><?= $data3['merk']?></div>
                                  <h3 class="tm-product-card-title">
                                    <a class="uk-link-heading" href="product.html"><?= $data3['seri']?> - <?= $data3['tahun']?></a>
                                    <div class="uk-text-meta uk-margin-xsmall-bottom">Rp <?= str_replace(",",".", number_format($data3['harga']))?></div>
                                  </h3>

                                  <ul class="uk-list uk-text-small tm-product-card-properties">
                                    <li><span class="uk-text-muted">Tipe: </span><span><?= $data3['tipe']?></span></li>
                                    <li><span class="uk-text-muted">CC: </span><span><?= str_replace(",",".", number_format($data3['CC']))?></span></li>
                                    <li><span class="uk-text-muted">Bahan Bakar: </span><span><?= $data3['bahan_bakar']?></span></li>
                                    <li><span class="uk-text-muted">Transmisi: </span><span><?= $data3['transmisi']?></span></li>
                                    <li><span class="uk-text-muted">Nilai: </span><span><?= $nilai[$data3['id_mobil']]?></span></li>
                                  </ul>
                                </div>
                                <div class="tm-product-card-shop" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
                                  <div class="tm-product-card-prices" style="flex-grow: 0;">
                                    <div class="tm-product-card-price">
                                      <small>Rp <?= str_replace(",",".", number_format($data3['harga']))?></small>
                                    </div>
                                  </div>
                                  <div class="tm-product-card-add">
                                    <button type="button" onclick="window.open('<?= $base_url ?>/detail.php?id=<?= $data3['id_mobil']?>', '_blank')" class="uk-button uk-button-primary tm-product-card-add-button tm-shine jss-add-to-cart">
                                      <span class="tm-product-card-add-button-icon" uk-icon="cart"></span><span class="tm-product-card-add-button-text">detail</span>
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </article>
                            <?php endwhile;?>

                          </div>
                        </div>
                        <?php endif;?>

                      </div>
                    </div>
                    <!-- /Hasil rekomendasi -->

                  </div>
                  <!-- /Left side -->
  
                </div>
              </div>
            </form>
            <!-- /Form Penilaian -->

          </div>
        </div>
      </section>

      <!-- Main Footer -->
      <?php include("./components/sub-main-footer.php") ?>
      <!-- /Main Footer -->
    </main>

    <!-- Footer -->
    <?php include("./components/footer.php") ?>
    <!-- /Footer -->

    <!-- Offcanvas -->
    <?php include("./components/offcanvas.php") ?>
    <!-- /Offcanvas -->

  </div>

  <!-- Modal data -->
  <?php
    $sql = "SELECT * FROM mobil";
    $result = $con->query($sql);
  ?>
  <!-- /Modal data -->

  <!-- Modal -->
  <div id="modal-example" class="uk-modal-full" uk-modal>
    <div style="position: fixed; bottom: 16px; right: 24px; z-index: 1000;">
      <div class="uk-button-group">
        <button type="button" class="uk-button uk-button-sondary" uk-close></button>
        <button type="submit" form="frmPilihMobil" class="uk-button uk-button-primary tm-shine uk-margin-small-left">Simpan</button>
      </div>
    </div>
    <div class="uk-modal-dialog uk-modal-body">
      
      <form action="" id="frmPilihMobil">
        <!-- post data -->
        <input name="choose_car" type="hidden" value="" />
        <!-- /post data -->

        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        <div class="uk-grid-collapse uk-child-width-1-3 stm-products-grid sjs-products-grid tm-products-list js-products-list" uk-grid>
  
          <?php $i = 1; while ($row = $result->fetch_assoc()) : ?>
          <article class="tm-product-card">
            <div class="tm-product-card-media">
              <div class="tm-ratio tm-ratio-4-3">
                <a class="tm-media-box">
                  <figure class="tm-media-box-wrap"><img src="<?= $base_url ?>/assets/images/cars/2_<?= $row['nopol'] ?>.jpg" alt='' /></figure>
                </a>
              </div>
            </div>
            <div class="tm-product-card-body">
              <div class="tm-product-card-info">
                <div class="uk-text-meta uk-margin-xsmall-bottom"><?= $row['merk'] ?></div>
                <h3 class="tm-product-card-title">
                  <a class="uk-link-heading" href="product.html"><?= $row['seri'] ?> - <?= $row['tahun'] ?></a>
                </h3>
                <ul class="uk-list uk-text-small tm-product-card-properties">
                  <li><span class="uk-text-muted">Tipe: </span><span><?= $row['tipe'] ?></span></li>
                  <li><span class="uk-text-muted">CC: </span><span><?= str_replace(",",".", number_format($row['CC'])) ?></span></li>
                  <li><span class="uk-text-muted">Bahan Bakar: </span><span><?= $row['bahan_bakar']?></span></li>
                  <li><span class="uk-text-muted">Transmisi: </span><span><?= $row['transmisi']?></span></li>
                </ul>
              </div>
              <div class="tm-product-card-shop" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
                <div class="tm-product-card-add">
                  <label class="uk-switch" for="A<?= $i?>">
                    <!-- post data -->
                    <input type="checkbox" name="A<?= $i?>" id="A<?= $i?>">
                    <!-- /post data -->
                    <div class="uk-switch-slider"></div>
                  </label>
                </div>
              </div>
            </div>
          </article>

          <input name="t1<?=$i?>" type="hidden" id="t1<?=$i?>" value="<?= $row['id_mobil'] ?>" />
          <input name="t2<?=$i?>" type="hidden" id="t2<?=$i?>" value="<?= $row['nopol'] ?>" />
          
          <?php $i++; endwhile; ?>
  
          <!-- post data -->
          <input name="t4" type="hidden" id="t4" value="<?=$i?>" />
          <!-- /post data -->
        
        </div>
      </form>

    </div>
  </div>
  <!-- /Modal -->

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

  <?php if(isset($_POST['id_mobil'])):?>
  <script>
    // <!-- recommend_product -->
    let recommend = document.getElementById("recommend_product");

    window.scroll({
      top: recommend.scrollHeight, 
      left: 0, 
      behavior: 'smooth' 
    });
  </script>
  <?php endif;?>

</body>

</html>