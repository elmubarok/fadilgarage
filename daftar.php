<?php include_once("./config.php");

  $failed = false;

  if(isset($_SESSION["role"])){
    $role = $_SESSION["role"];

    header("Location: ".$base_url."/admin.php");
  }

  if(isset($_POST['t11'])){
    $user = $_POST['t11'];
    $pass = $_POST['t12'];
    $email = $_POST['t13'];
    $ym = $_POST['t14'];
    $alamat = $_POST['t15'];
    $telp = $_POST['t16'];

    $sql = "INSERT INTO `informer` (`user`, `pass`, `email`, `ym`, `alamat`, `telp`) VALUES ('$user', '$pass', '$email', '$ym', '$alamat', '$telp')";
    $jalan = $con->query($sql);

    header("Location: ".$base_url."/login.php");
  }

?>

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->

<body>
  <div class="">

    <main>
      <section class="uk-section uk-section-small">

        <div class="uk-container uk-flex uk-flex-center uk-flex-middle uk-flex-column">
          
          <img src="<?= $base_url ?>/assets/images/logo-2.png" class="uk-margin-medium">

          <div class="uk-child-width-1-1s uk-child-width-1-4@ms uk-margin-small">

            <form action="" class="uk-form-stacked" method="post">

              <div class="uk-margin-small">
                <label class="uk-form-label">Username</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: user"></span>
                  <input autofocus class="uk-input" name="t11" type="text" placeholder="...">
                </div>
              </div>

              <div class="uk-margin">
                <label class="uk-form-label">Password</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: unlock"></span>
                  <input class="uk-input" name="t12" type="password" placeholder="...">
                </div>
              </div>

              <div class="uk-margin">
                <label class="uk-form-label">Email</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: mail"></span>
                  <input class="uk-input" name="t13" type="email" placeholder="...">
                </div>
              </div>

              <div class="uk-margin">
                <label class="uk-form-label">YM</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: nut"></span>
                  <input class="uk-input" name="t14" type="text" placeholder="...">
                </div>
              </div>

              <div class="uk-margin">
                <label class="uk-form-label">Alamat</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: home"></span>
                  <input class="uk-input" name="t15" type="text" placeholder="...">
                </div>
              </div>

              <div class="uk-margin">
                <label class="uk-form-label">Telp.</label>
                <div class="uk-inline uk-form-controls uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: receiver"></span>
                  <input class="uk-input" name="t16" type="text" placeholder="...">
                </div>
              </div>
              
              <div class="uk-margin">
                <button class="uk-button uk-button-primary uk-width-1-1" 
                type="submit">
                  Daftar
                </button>
              </div>
              <div class="uk-margin">
                <a href="<?= $base_url?>/login.php" class="uk-button uk-button-default uk-width-1-1" >
                  Login
                </a>
              </div>
            </form>
            
          </div>

        </div>
      </section>

    </main>

  </div>

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

</body>

</html>