<?php include_once("./config.php")?>
<!DOCTYPE html>
<html lang="en">

  <!-- Head -->
  <?php include("./components/head.php") ?>
  <!-- /Head -->

  <body>
    <div class="uk-offcanvas-content">

      <!-- Header -->
      <?php include("./components/header.php") ?>
      <!-- /Header -->

      <?php
        $id = $_GET['id'];
        $tampil = "SELECT * FROM mobil WHERE id_mobil='$id'";
        $hasil = $con->query($tampil);
        $data = $hasil->fetch_assoc();

        $tampil1 = "SELECT * FROM informer WHERE user='".$data['informer']."'";
        $hasil1 = $con->query($tampil1);
        $data1 = $hasil1->fetch_assoc();
      ?>

      <main>
        <section class="uk-section uk-section-small">
          <div class="uk-container">
            <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
              <div>
                <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
                  <div>
                    <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                      <div class="uk-grid-small uk-grid-collapse uk-grid-match" uk-grid>
                        <div class="uk-width-1-1 uk-width-expand@m">
                          <div class="uk-grid-collapse uk-child-width-1-1" uk-slideshow="finite: true; ratio: 4:3;" uk-grid>
                            <div>
                              <ul class="uk-slideshow-items" uk-lightbox>
                                <?php for($i=1; $i<=4; $i++):?>
                                <li>
                                  <a class="uk-card-body tm-media-box tm-media-box-zoom" href="<?= $base_url?>/assets/images/cars/<?= $i?>_<?= $data['nopol']?>.jpg">
                                    <figure class="tm-media-box-wrap"><img src="<?= $base_url?>/assets/images/cars/<?= $i?>_<?= $data['nopol']?>.jpg" alt='' /></figure>
                                  </a>
                                </li>
                                <?php endfor;?>
                              </ul>
                            </div>
                            <div>
                              <div class="uk-card-body uk-flex uk-flex-center">
                                <div class="uk-width-1-2 uk-visible@s">
                                  <div uk-slider="finite: true">
                                    <div class="uk-position-relative">
                                      <div class="uk-slider-container">
                                        <ul class="tm-slider-items uk-slider-items uk-child-width-1-4 uk-grid uk-grid-small">
                                          <?php for($i=1; $i<=4; $i++):?>
                                          <li uk-slideshow-item="<?= $i-1?>">
                                            <div class="tm-ratio tm-ratio-1-1">
                                              <a class="tm-media-box tm-media-box-frame">
                                                <figure class="tm-media-box-wrap"><img src="<?= $base_url?>/assets/images/cars/<?= $i?>_<?= $data['nopol']?>.jpg" alt='' /></figure>
                                              </a>
                                            </div>
                                          </li>
                                          <?php endfor;?>
                                        </ul>
                                        <div>
                                          <a class="uk-position-center-left-out uk-position-small" href="#" uk-slider-item="previous" uk-slidenav-previous></a>
                                          <a class="uk-position-center-right-out uk-position-small" href="#" uk-slider-item="next" uk-slidenav-next></a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <ul class="uk-slideshow-nav uk-dotnav uk-hidden@s"></ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-3@m tm-product-info">
                          <div class="uk-card-body">

                            <div class="uk-margin-medium">
                              <!-- <p class="uk-text-muted" style="margin-bottom: 0;">Marcedes Benz</p> -->
                              <img src="<?= $base_url?>/assets/images/brands/cars/<?= $data['merk']?>.png" style="height: 40px;">
                              <p class="uk-card-title">
                                <?= $data['merk']?> <?= $data['seri']?> - <?= $data['tahun']?>
                              </p>
                            </div>

                            <!-- Price -->
                            <div class="uk-margin">
                              <div class="uk-padding-small uk-background-primary-lighten uk-border-rounded">
                                <div class="uk-grid-small uk-child-width-1-1" uk-grid>
                                  <div>
                                    <div class="tm-product-price">Rp <?= str_replace(",",".", number_format($data['harga']))?></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                            <div class="uk-margin-medium">
                              <ul class="uk-list uk-text-smalls uk-list-large uk-list-divider uk-margin-remove">
                                <li><span class="uk-text-muted">Tipe: </span><span><?= $data['tipe']?></span></li>
                                <li><span class="uk-text-muted">CC: </span><span><?= str_replace(",",".", number_format($data['CC']))?></span></li>
                                <li><span class="uk-text-muted">Transmisi: </span><span><?= $data['transmisi']?></span></li>
                                <li><span class="uk-text-muted">Bahan Bakar: </span><span><?= $data['bahan_bakar']?></span></li>
                                <li><span class="uk-text-muted">Warna: </span><span><?= $data['warna']?></span></li>
                                <li><span class="uk-text-muted">Keterangan: </span><span><?= $data['ket']?></span></li>
                              </ul>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
                <div id="review" uk-modal>
                  <div class="uk-modal-dialog uk-modal-body">
                    <button class="uk-modal-close-outside" type="button" uk-close></button>
                    <h2 class="uk-modal-title uk-text-center">Review</h2>
                    <form class="uk-form-stacked">
                      <div class="uk-grid-small uk-child-width-1-1" uk-grid>
                        <div>
                          <label>
                            <div class="uk-form-label uk-form-label-required">Name</div>
                            <input class="uk-input" type="text" required />
                          </label>
                        </div>
                        <div>
                          <label>
                            <div class="uk-form-label uk-form-label-required">Email</div>
                            <input class="uk-input" type="email" required />
                          </label>
                        </div>
                        <div>
                          <div class="uk-form-label">Rating</div>
                          <ul class="uk-iconnav tm-rating">
                            <li><a uk-icon="star"></a></li>
                            <li><a uk-icon="star"></a></li>
                            <li><a uk-icon="star"></a></li>
                            <li><a uk-icon="star"></a></li>
                            <li><a uk-icon="star"></a></li>
                          </ul>
                        </div>
                        <div>
                          <label>
                            <div class="uk-form-label uk-form-label-required">Review</div>
                            <textarea class="uk-textarea" rows="5" required></textarea>
                          </label>
                        </div>
                        <div class="uk-text-center"><button class="uk-button uk-button-primary">Send</button></div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        
        <!-- Main Footer -->
        <?php include("./components/sub-main-footer.php") ?>
        <!-- /Main Footer -->

      </main>

      <!-- Footer -->
      <?php include("./components/footer.php") ?>
      <!-- /Footer -->

      <!-- Offcanvas -->
      <?php include("./components/offcanvas.php") ?>
      <!-- /Offcanvas -->

    </div>

    <!-- Script -->
    <?php include("./components/script.php") ?>
    <!-- /Script -->

  </body>
</html>
