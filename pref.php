<?php //include_once("./config.php");

  session_start();
  
  $base_url = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
  $base_url .= $_SERVER['HTTP_HOST'] . "/new";

  if (isset($_SESSION['pref'])) {
    header("Location: ".$base_url."/spk.php");
  }

  $terms = [
    ["display" => "Harga", "name" => "t1"],
    ["display" => "Desain", "name" => "t2"],
    ["display" => "CC", "name" => "t3"],
    ["display" => "Bahan Bakar", "name" => "t4"],
    ["display" => "Transmisi", "name" => "t5"]
  ];
  
  $terms_val = [
    "3" => "Penting",
    "2" => "Ragu - Ragu",
    "1" => "Tidak Penting"
  ];

  if($_POST){
    $_SESSION['pref'] = array_values($_POST);
    header("Location: ".$base_url."/spk.php");
  }

?>

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->


<body>
  <div class="">

    <main>
      <section class="uk-section uk-section-small">

        <div class="uk-container uk-flex uk-flex-center uk-flex-middle uk-flex-column">

          <img src="<?= $base_url ?>/assets/images/logo-2.png" class="uk-margin-medium">

          <div class="">
            <h4 class="uk-text-center">Silahkan pilih preferensi anda dalam memilih mobil.</h4>

            <form action="" class="uk-margin-large" method="post">
              <div class="uk-child-width-1-5 uk-grid-large" uk-grid>

                <?php for ($i = 0; $i < count($terms); $i++) : ?>
                  <div>
                    <p><?= $terms[$i]['display']?></p>
                    <div class="uk-margin">
                      <select class="uk-select" name="<?= $terms[$i]['name']?>">
                        <option value="3">Penting</option>
                        <option value="2">Ragu - Ragu</option>
                        <option value="1">Tidak Penting</option>
                      </select>
                    </div>
                  </div>
                <?php endfor; ?>
              </div>

              <div class="uk-margin-large">
                <button class="uk-button uk-button-primary uk-width-1-1" type="submit">
                  Simpan
                </button>
              </div>
            </form>

          </div>

        </div>
      </section>

    </main>

  </div>

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

</body>

</html>