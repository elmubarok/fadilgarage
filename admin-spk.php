<?php
  include_once("./config.php");

  if(!isset($_SESSION["role"])){
    header("Location: ".$base_url);
  }

  if($_SESSION["role"] != "admin"){
    header("Location: ".$base_url.'/admin.php');
  }

  // post method exists
  if(isset($_POST["t4"])){
    $total_i = $_POST['t4'];

    for ($i = 1; $i < $total_i; $i++) {
      $idmobil[$i] = $_POST["t0$i"];
      $nopol[$i] = $_POST["t1$i"];
      $merk[$i] = $_POST["t2$i"];

      $harga[$i] = $_POST["s1$i"];
      $desain[$i] = $_POST["s2$i"];
      $mesin[$i] = $_POST["s3$i"];
      $aman[$i] = $_POST["s4$i"];
      $nyaman[$i] = $_POST["s5$i"];

      $tampil = "SELECT * FROM penilaian";
      $hasil = $con->query($tampil);
      $sql = "INSERT INTO `penilaian` (`id_mobil`, `nopol`, `merk`, `harga`, `desaign`, `mesin`, `aman`, `nyaman`) VALUES ('$idmobil[$i]', '$nopol[$i]', '$merk[$i]', '$harga[$i]', '$desain[$i]', '$mesin[$i]', '$aman[$i]', '$nyaman[$i]')";
      $jalan = $con->query($sql);
      $sql1 = "UPDATE `mobil` SET  `st_nilai` = 'Sudah' WHERE `id_mobil` = '$idmobil[$i]' ";
      $hasil = $con->query($sql1);
    }

    header("Location: ".$base_url."/admin-spk.php");
  }
?>

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->

<body>
  <div class="uk-offcanvas-content">

    <?php
      $userid = $_SESSION['userid'];
      $sql = "SELECT * FROM mobil WHERE st_nilai = 'Belum' ORDER BY id_mobil DESC";
      $result = $con->query($sql);
    ?>

    <main>
      <section class="uk-section uk-section-small">
        <div class="uk-container">

          <h3>Penilaian Kriteria.</h3>

          <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

            <!-- User Info -->
            <?php include("./components/user-info.php") ?>
            <!-- User Info -->

            <div class="uk-grid-margin uk-first-column uk-width-1-1 uk-width-expand@m">
              <div class="uk-grid-medium" uk-grid>

                <div class="uk-width-expand">
                  <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

                    <div class="uk-first-column">

                      <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                        <div class="uk-grid-collapse uk-child-width-1-1" id="products" uk-grid>

                          <div class="uk-card-header uk-flex uk-flex-right">
                            <button type="submit" form="frmNilai" class="uk-button uk-button-small uk-button-primary tm-shine">simpan</button>
                          </div>

                          <div>
                            <div class="uk-grid-collapses uk-child-width-1-3s" uk-grid>

                              <form action="" id="frmNilai" method="POST">
                                <div class="uk-overflow-auto uk-width-1-1">
                                  <table class="uk-table uk-table-small uk-table-hover uk-table-middle uk-table-divider">
                                    <thead>
                                      <tr>
                                        <th style="vertical-align: middle; text-align: center;" rowspan="2">Mobil</th>
                                        <th style="vertical-align: middle; text-align: center;" class="uk-table-expand" colspan="5">Kriteria</th>
                                      </tr>
                                      <tr>
                                        <th>Harga</th>
                                        <th>Design</th>
                                        <th>Mesin</th>
                                        <th>Keamanan</th>
                                        <th>Kenyamanan</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=1; while ($row = $result->fetch_assoc()) : ?>
  
                                      <input name="t0<?=$i?>" type="hidden" value="<?= $row['id_mobil'] ?>" />
                                      <input name="t1<?=$i?>" type="hidden" value="<?= $row['nopol'] ?>" />
                                      <input name="t2<?=$i?>" type="hidden" value="<?= $row['merk'] ?>" />
                                      
                                      <tr>
                                        <td class="uk-text-nowrap">
                                          <div class="uk-width-1-1 uk-flex uk-flex-middle">
                                            <img class="uk-margin-top uk-margin-left uk-margin-right uk-margin-bottom" 
                                              style="width: 100px; height: 80px; object-fit: cover;" 
                                              src="<?= $base_url ?>/assets/images/cars/2_<?= $row['nopol'] ?>.jpg" />
                                            <div>
                                              <div class="uk-text-meta uk-margin-xsmall-bottom"><?= $row['merk'] ?></div>
                                              <h3 style="white-space: normal;" class="tm-product-card-title uk-margin-remove-bottom">
                                                <span class="uk-link-heading"><?= $row['seri'] ?> - <?= $row['tahun'] ?></span>
                                              </h3>
                                            </div>
                                          </div>
                                        </td>
                                        <td>
                                          <select style="width: 100px;" class="uk-select uk-form-small" name="s1<?=$i?>">
                                            <option value="1">Sgt Mahal</option>
                                            <option value="2">Mahal</option>
                                            <option value="3">Sedang</option>
                                            <option value="4">Murah</option>
                                            <option value="5">Sgt Murah</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select style="width: 100px;" class="uk-select uk-form-small" name="s2<?=$i?>">
                                            <option value="1">Sgt Buruk</option>
                                            <option value="2">Buruk</option>
                                            <option value="3">Sedang</option>
                                            <option value="4">Baik</option>
                                            <option value="5">Sgt Baik</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select style="width: 100px;" class="uk-select uk-form-small" name="s3<?=$i?>">
                                            <option value="1">Sgt Buruk</option>
                                            <option value="2">Buruk</option>
                                            <option value="3">Sedang</option>
                                            <option value="4">Baik</option>
                                            <option value="5">Sgt Baik</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select style="width: 100px;" class="uk-select uk-form-small" name="s4<?=$i?>">
                                            <option value="1">Sgt Buruk</option>
                                            <option value="2">Buruk</option>
                                            <option value="3">Sedang</option>
                                            <option value="4">Baik</option>
                                            <option value="5">Sgt Baik</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select style="width: 100px;" class="uk-select uk-form-small" name="s5<?=$i?>">
                                            <option value="1">Sgt Buruk</option>
                                            <option value="2">Buruk</option>
                                            <option value="3">Sedang</option>
                                            <option value="4">Baik</option>
                                            <option value="5">Sgt Baik</option>
                                          </select>
                                        </td>
                                      </tr>
                                      <?php $i++; endwhile; ?>

                                      <input name="t4" type="hidden" value="<?=$i?>" />

                                    </tbody>
                                  </table>
                                </div>
                              </form>

                            </div>
                          </div>

                        </div>
                      </div>

                    </div>

                  </div>
                </div>

              </div>
            </div>

          </div>

        </div>
      </section>

    </main>

    <!-- Footer -->
    <?php include("./components/footer.php") ?>
    <!-- /Footer -->

    <!-- Offcanvas -->
    <?php include("./components/offcanvas.php") ?>
    <!-- /Offcanvas -->

  </div>

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

</body>

</html>