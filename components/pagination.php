<?php
  $tampil2 = !isset($_GET['filter']) ? "SELECT * FROM mobil" : $sql_paging;

  if(isset($_GET['search'])) $tampil2 = "SELECT * FROM mobil WHERE merk LIKE '%$search%' OR seri LIKE '%$search%' OR tipe LIKE '%$search%' OR tahun='$search' OR km='$search' OR harga='$search'";
  
  $hasil2 = $con->query($tampil2);
  $jmldata = mysqli_num_rows($hasil2);
  $jmlhalaman = ceil($jmldata/$batas);

  // echo $jmldata;

  $paginurl = "";

  if(isset($_GET['filter']) || isset($_GET['search'])){
    $paginurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $paginurl = explode("&", explode("?", $_SERVER['REQUEST_URI'])[1]);
    
    if(isset($_GET['halaman'])){
      $unset_pos = array_keys(preg_grep('/^halaman=\d.*/', $paginurl));
      unset($paginurl[$unset_pos[0]]);
    }

    $paginurl = "?" . implode("&", $paginurl) . "&";
  }else{
    $paginurl = "?";
  }
?>

<div class="uk-grid-margin uk-first-column">
  <ul class="uk-pagination uk-flex-center">
    <?php if($halaman != 1):?>
    <li>
      <a href="<?= $base_url . $paginurl . 'halaman=' . ((isset($_GET['halaman']) ? $_GET['halaman'] : 1) - 1)?>"><span uk-pagination-previous></span></a>
    </li>
    <?php endif;?>

    <?php for ($i=1;$i<=$jmlhalaman;$i++) if ($i != $halaman):?>
    <li><a href="<?= $base_url . $paginurl . 'halaman=' . $i?>"><?= $i?></a></li>
    <?php else:?>
    <li class="uk-active"><span><?= $i?></span></li>
    <?php endif;?>

    <?php if($halaman != $jmlhalaman):?>
    <li>
      <a href="<?= $base_url . $paginurl . 'halaman=' . ((isset($_GET['halaman']) ? $_GET['halaman'] : 1) + 1)?>"><span uk-pagination-next></span></a>
    </li>
    <?php endif;?>
    
    <!-- <li class="uk-disabled"><span>…</span></li> -->
  </ul>
</div>