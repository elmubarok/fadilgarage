<?php

  $q_brands = "SELECT merk, COUNT(merk) AS jumlah FROM mobil GROUP BY merk";
  $q_cities = "SELECT kota FROM mobil GROUP BY kota";
  
  $merk = $con->query($q_brands);
  $kota = $con->query($q_cities);

  $price_start = "";
  $price_end = "";
  $city = "";
  $brands = [];

  // filter
  if(isset($_GET['filter'])){
    $price_start = str_replace(".","",$_GET['price_start']);
    $price_end = str_replace(".","",$_GET['price_end']);
    $city = $_GET['city'];
    $brands = isset($_GET['brand']) ? $_GET['brand'] : [];
  }

?>

<aside class="uk-width-1-4 tm-aside-column tm-filters" id="filters" uk-offcanvas="overlay: true; container: false;">
  <div class="uk-offcanvas-bar uk-padding-remove">
    <div class="uk-card uk-card-default uk-card-small uk-flex uk-flex-column uk-height-1-1">
      <header class="uk-card-header uk-flex uk-flex-middle">
        <div class="uk-grid-small uk-flex-1" uk-grid>
          <div class="uk-width-expand">
            <h3>Filters</h3>
          </div>
          <button class="uk-offcanvas-close" type="button" uk-close></button>
        </div>
      </header>

      <form action="/new">
        <div class="uk-margin-remove uk-flex-1 uk-overflow-auto" uk-accordion="multiple: true; targets: &gt; .js-accordion-section" style="flex-basis: auto;">
  
          <input type="number" value="" name="filter" hidden>
          <!-- Harga -->
          <section class="uk-card-body uk-open js-accordion-section">
            <h4 class="uk-accordion-title uk-margin-remove">Range Harga</h4>
            <div class="uk-accordion-content">
              <div class="uk-grid-small uk-child-width-1-1 uk-text-small uk-margin-tops" uk-grid>
                <div>
                  <div class="uk-inline"><span class="uk-form-icon uk-text-xsmall">dari</span><input class="uk-input price-range" type="text" name="price_start" value="<?= $price_start?>" placeholder="..." /></div>
                </div>
                <div>
                  <div class="uk-inline"><span class="uk-form-icon uk-text-xsmall">ke</span><input class="uk-input price-range" type="text" name="price_end" value="<?= $price_end?>" placeholder="..." /></div>
                </div>
              </div>
            </div>
          </section>
  
          <!-- Merk -->
          <section class="uk-card-body js-accordion-section uk-open">
            <h4 class="uk-accordion-title uk-margin-remove">Merek</h4>
            <div class="uk-accordion-content">
              <ul class="uk-list tm-scrollboxs">
                <?php while ($row = $merk->fetch_assoc()) : ?>
                  <li>
                    <input <?= $brands ? (in_array($row['merk'], $brands) ? 'checked' : '') : ''?> class="tm-checkbox" id="<?= $row['merk']?>" name="brand[]" value="<?= $row['merk']?>" type="checkbox" />
                    <label for="<?= $row['merk']?>">
                      <span><?= $row['merk']?> <span class="uk-text-meta uk-text-xsmall"><?= $row['jumlah']?></span></span>
                    </label>
                  </li>
                <?php endwhile; ?>
              </ul>
            </div>
          </section>
  
          <!-- Kota -->
          <section class="uk-card-body js-accordion-section uk-open">
            <h4 class="uk-accordion-title uk-margin-remove">
              Kota<span class="tm-help-icon" uk-icon="icon: question; ratio: .75;" onclick="event.stopPropagation();"></span>
              <div class="uk-margin-remove" uk-drop="mode: click;boundary-align: true; boundary: !.uk-accordion-title; pos: bottom-justify;">
                <div class="uk-card uk-card-body uk-card-default uk-card-small uk-box-shadow-xlarge uk-text-small">A small description for this property</div>
              </div>
            </h4>
            <div class="uk-accordion-content">
              <div class="uk-margin">
                <select class="uk-select" name="city">
                  <option value="">Semua</option>
                  <?php while ($row = $kota->fetch_assoc()) : ?>
                  <option <?= $city == $row['kota'] ? 'selected' : ''?> value="<?= $row['kota']?>"><?= $row['kota']?></option>
                  <?php endwhile; ?>
                </select>
              </div>
            </div>
          </section>
  
          <div class="uk-card-body">
            <button class="uk-button uk-button-default uk-width-1-1"><span class="uk-margin-xsmall-right" uk-icon="icon: check; ratio: .75;"></span>Terapkan Filter</button>
          </div>
  
        </div>
      </form>

    </div>
  </div>
</aside>