<div id="cart-offcanvas" uk-offcanvas="overlay: true; flip: true" style="z-index: 1000000;">
  <aside class="uk-offcanvas-bar uk-padding-remove">
    <div class="uk-card uk-card-default uk-card-small uk-height-1-1 uk-flex uk-flex-column tm-shadow-remove">
      <header class="uk-card-header uk-flex uk-flex-middle">
        <div class="uk-grid-small uk-flex-1" uk-grid>
          <div class="uk-width-expand">
            <div class="uk-h3">Login</div>
          </div>
          <button class="uk-offcanvas-close" type="button" uk-close></button>
        </div>
      </header>

      <div class="uk-card-body uk-overflow-auto">
        <div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-1@m uk-margin-small" uk-grid>

          <form action="" class="uk-form-stacked">
            <div class="uk-margin-small">
              <label class="uk-form-label">Username</label>
              <div class="uk-inline uk-form-controls uk-width-1-1">
                <span class="uk-form-icon" uk-icon="icon: user"></span>
                <input class="uk-input" type="text" placeholder="...">
              </div>
            </div>
  
            <div class="uk-margin">
              <label class="uk-form-label">Password</label>
              <div class="uk-inline uk-form-controls uk-width-1-1">
                <span class="uk-form-icon" uk-icon="icon: unlock"></span>
                <input class="uk-input" type="password" placeholder="...">
              </div>
            </div>
  
            <div class="uk-margin">
              <a class="uk-button uk-button-primary uk-width-1-1" href="checkout.html">Login</a>
            </div>
          </form>

        </div>
      </div>

    </div>
  </aside>
</div>