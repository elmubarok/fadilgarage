<!-- User Info -->
<div class="uk-width-1-1 uk-width-1-5@m tm-aside-column">
  <!-- <h2>Hello Admin.</h2> -->
  <!-- <h2><?= isset($_GET['edit']) ? 'Ubah' : 'Tambah'?> Data.</h2> -->
  <div class="uk-card uk-card-default uk-card-small tm-ignore-container" uk-sticky="offset: 20; bottom: true; media: @m;">
    <div class="uk-card-header">
      <div class="uk-grid-small uk-child-width-1-1" uk-grid>
        <section>
          <div class="uk-width-1-3 uk-width-1-4@s uk-width-1-2@m uk-margin-auto uk-visible-toggle uk-position-relative uk-border-circle uk-overflow-hidden uk-light">
            <img class="uk-width-1-1" src="<?= $base_url ?>/assets/images/about/martin.svg">
          </div>
        </section>
        <div class="uk-text-center">
          <div class="uk-h4 uk-margin-remove"><?= $_SESSION['userid'] ?></div>
          <div class="uk-text-meta"><?= $_SESSION['email'] ?></div>
        </div>
        <div>
          <div class="uk-grid-small uk-flex-center" uk-grid>
            <div>
              <button onclick="window.open('<?= $base_url.'?logout='?>', '_self')" class="uk-button uk-button-default uk-button-small" title="Log out">
                <span uk-icon="icon: sign-out; ratio: .75;"></span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div>
      <nav>
        <ul class="uk-nav uk-nav-default tm-nav">
          <li>
            <a href="<?= $base_url ?>/admin.php">
              Beranda
            </a>
          </li>
          <li>
            <a href="<?= $base_url ?>/crud.php">
              Tambah Data
            </a>
          </li>
          <?php if($_SESSION['role'] == "admin"):?>
          <li class="uk-actives">
            <a href="<?= $base_url ?>/admin-spk.php">
              Penilaian Kriteria
            </a>
          </li>
          <?php endif;?>
        </ul>
      </nav>
    </div>
  </div>
  <div class="uk-sticky-placeholder" style="margin: 0px;" hidden=""></div>
</div>
<!-- User Info -->