<a href="#" id="toTop" class="uk-position-small uk-position-center-right uk-position-fixed" style="visibility: hidden; transition: .3s all; opacity: 0;" uk-totop uk-scroll></a>

<script src="<?= $base_url ?>/assets/scripts/OverlayScrollbars.min.js"></script>
<script src="<?= $base_url ?>/assets/scripts/script.js"></script>
<script src="<?= $base_url ?>/assets/scripts/nouislider.min.js"></script>
<script src="<?= $base_url ?>/assets/scripts/inputmask.min.js"></script>
<!-- <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyClyjCemJi4m2q78gNeGkhlckpdH1hzTYA&amp;callback=initMap" async defer></script> -->
<script>
  // show / hide button to top
  let totop = document.querySelector("#toTop");
  document.addEventListener("scroll", function(e) {
    // console.log(e.pageYOffset);
    if (e.pageYOffset > 300) {
      totop.style.opacity = 1;
      totop.style.visibility = "visible";
    } else {
      totop.style.opacity = 0;
      totop.style.visibility = "hidden";
    }
  });
  // end show / hide button to top

  let price_range = document.querySelectorAll(".price-range");
  price_range.forEach(function(e,i){
    Inputmask({
      mask: "999.999.999"
    }).mask(e);
  });
</script>