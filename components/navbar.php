<header>
  <div class="container">
    <div class="row">
      <div id="header">
        <div class="header-container">
          <div class="header-logo"> <a href="index-2.html" title="Car HTML" class="logo">
              <div><img src="<?= $base_url ?>/assets/images/logo.png" alt="Car Store"></div>
            </a> </div>
          <div class="header__nav">
            <div class="header-banner">
              <div class="col-lg-6 col-xs-12 col-sm-6 col-md-6">
                <div class="assetBlock">
                  <div style="height: 20px; overflow: hidden;" id="slideshow">
                    <p style="display: block;">Hot days! - <span>50%</span> Get ready for summer! </p>
                    <p style="display: none;">Save up to <span>40%</span> Hurry limited offer!</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-lg-6 col-xs-12 col-sm-6 col-md-6 call-us"><i class="fa fa-clock-o"></i> Mon - Fri : 09am to 06pm <i class="fa fa-phone"></i> +1 800 789 0000</div>
            </div>
            <div class="fl-header-right">
              <div class="fl-links">
                <div class="no-js"> <a title="" class="clicker"></a>
                  <div class="fl-nav-links">
                    <h3>My Acount</h3>
                    <ul class="links">
                      <li><a href="login.html" title="My Account">Login</a></li>
                      <li><a href="login.html" title="Wishlist">Register</a></li>
                    </ul>
                  </div>
                </div>
              </div>

              <!-- Search -->
              <div class="collapse navbar-collapse">
                <form class="navbar-form" role="search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-btn">
                      <button type="submit" class="search-btn"> <span class="glyphicon glyphicon-search"> <span class="sr-only">Search</span> </span> </button>
                    </span> </div>
                </form>
              </div>

            </div>

            <!-- Navbar -->
            <div class="fl-nav-menu">
              <nav>
                <div class="mm-toggle-wrap">
                  <div class="mm-toggle"><i class="fa fa-bars"></i><span class="mm-label">Menu</span> </div>
                </div>
                <div class="nav-inner">
                  <!-- BEGIN NAV -->
                  <ul id="nav" class="hidden-xs">
                    <li class="active"> <a class="level-top" href="#"><span>Home</span></a></li>
                    <li class="mega-menu"> <a class="level-top" href="grid1.html"><span>Accessories</span></a>
                      <div class="level0-wrapper dropdown-6col" style="left: 0px; display: none;">
                        <div class="container">
                          <div class="level0-wrapper2">
                            <div class="nav-block nav-block-center">
                              <!--mega menu-->
                              <ul class="level0">
                                <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Audio</span></a>
                                  <!--sub sub category-->
                                  <ul class="level1">
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Amplifiers</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Installation Parts</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Speakers</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Stereos</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Subwoofers</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                  </ul>
                                  <!--level1-->
                                  <!--sub sub category-->
                                </li>
                                <!--level3 nav-6-1 parent item-->
                                <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Body Parts</span></a>
                                  <!--sub sub category-->
                                  <ul class="level1">
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Bumpers</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Doors</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Fenders</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Grilles</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Hoods</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                  </ul>
                                  <!--level1-->
                                  <!--sub sub category-->
                                </li>
                                <!--level3 nav-6-1 parent item-->
                                <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Exterior</span></a>
                                  <!--sub sub category-->
                                  <ul class="level1">
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Bed Accessories</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Body Kits</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Custom Grilles</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Car Covers</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Off-Road Bumpers</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                  </ul>
                                  <!--level1-->
                                  <!--sub sub category-->
                                </li>
                                <!--level3 nav-6-1 parent item-->
                                <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Interior</span></a>
                                  <!--sub sub category-->
                                  <ul class="level1">
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Custom Gauges</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Dash Kits</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Seat Covers</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Steering Wheels</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Sun Shades</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                  </ul>
                                  <!--level1-->
                                  <!--sub sub category-->
                                </li>
                                <!--level3 nav-6-1 parent item-->
                                <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Lighting</span></a>
                                  <!--sub sub category-->
                                  <ul class="level1">
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Fog Lights</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Headlights</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>LED Lights</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Off-Road Lights</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Signal Lights</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                  </ul>
                                  <!--level1-->
                                  <!--sub sub category-->
                                </li>
                                <!--level3 nav-6-1 parent item-->
                                <li class="level3 nav-6-1 parent item"> <a href="grid.html"><span>Performance</span></a>
                                  <!--sub sub category-->
                                  <ul class="level1">
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Air Intake Systems</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Brakes</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Exhaust Systems</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Power Adders</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                    <li class="level2 nav-6-1-1"> <a href="grid.html"><span>Racing Gear</span></a> </li>
                                    <!--level2 nav-6-1-1-->
                                  </ul>
                                  <!--level1-->
                                  <!--sub sub category-->
                                </li>
                                <!--level3 nav-6-1 parent item-->
                              </ul>
                              <!--level0-->
                            </div>
                            <!--nav-block nav-block-center-->

                          </div>
                          <!--level0-wrapper2-->
                        </div>
                        <!--container-->
                      </div>
                      <!--level0-wrapper dropdown-6col-->
                      <!--mega menu-->
                    </li>
                    <li class="level0 parent drop-menu"> <a class="level-top" href="#"><span>Listing‎</span></a>
                      <ul class="level1">
                        <li class="level1 first"><a href="grid.html"><span>Car Grid</span></a></li>
                        <li class="level1 nav-10-2"> <a href="list.html"> <span>Car List</span> </a> </li>
                        <li class="level1 nav-10-3"> <a href="grid1.html"> <span>Accessories Grid</span> </a> </li>
                        <li class="level1 nav-10-4"> <a href="list1.html"> <span>Accessories List</span> </a> </li>
                        <li class="level1 first parent"><a href="car-detail.html"><span>Car Detail</span></a> </li>
                        <li class="level1 first parent"><a href="accessories-detail.html"><span>Accessories Detail</span></a> </li>
                      </ul>
                    </li>
                    <li class="level0 parent drop-menu"> <a class="level-top" href="#"><span>Blog</span></a>
                      <ul class="level1">
                        <li class="level1 first"><a href="blog.html"><span>Blog List</span></a></li>
                        <li class="level1 nav-10-2"> <a href="blog-detail.html"> <span>Blog Detail</span> </a> </li>
                      </ul>
                    </li>
                    <li class="mega-menu hidden-sm"> <a class="level-top" href="compare.html"><span>Compare Cars‎</span></a> </li>
                    <li class="level0 parent drop-menu"><a href="#"><span>Pages</span> </a>
                      <!--sub sub category-->
                      <ul class="level1">
                        <li class="level1"> <a href="about-us.html"> <span>About us</span> </a> </li>
                        <li class="level1 nav-10-4"> <a href="shopping-cart.html"> <span>Cart Page</span> </a> </li>
                        <li class="level1 first parent"><a href="checkout.html"><span>Checkout</span></a>
                          <!--sub sub category-->
                          <ul class="level2 right-sub">
                            <li class="level2 nav-2-1-1 first"><a href="checkout-method.html"><span>Method</span></a></li>
                            <li class="level2 nav-2-1-5 last"><a href="checkout-billing-info.html"><span>Billing Info</span></a></li>
                          </ul>
                          <!--sub sub category-->
                        </li>
                        <li class="level1 nav-10-4"> <a href="wishlist.html"> <span>Wishlist</span> </a> </li>
                        <li class="level1"> <a href="dashboard.html"> <span>Dashboard</span> </a> </li>
                        <li class="level1"> <a href="multiple-addresses.html"> <span>Multiple Addresses</span> </a> </li>
                        <li class="level1"><a href="contact-us.html"><span>Contact us</span></a> </li>
                        <li class="level1"><a href="404error.html"><span>404 Error Page</span></a> </li>
                        <li class="level1"><a href="login.html"><span>Login Page</span></a> </li>
                        <li class="level1"><a href="quickview.html"><span>Quick View</span></a> </li>
                        <li class="level1"><a href="newsletter.html"><span>Newsletter</span></a> </li>
                      </ul>
                    </li>
                    <li class="fl-custom-tabmenulink mega-menu"> <a href="#" class="level-top"> <span>Custom</span> </a>
                      <div class="level0-wrapper fl-custom-tabmenu" style="left: 0px; display: none;">
                        <div class="container">
                          <div class="header-nav-dropdown-wrapper clearer">
                            <div class="grid12-3">
                              <div><img src="<?= $base_url ?>/assets/images/custom-img1.jpg" alt="custom-image"></div>
                              <h4 class="heading">SALE UP TO 30% OFF</h4>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="grid12-3">
                              <div><img src="<?= $base_url ?>/assets/images/custom-img2.jpg" alt="custom-image"></div>
                              <h4 class="heading">SALE UP TO 30% OFF</h4>
                              <p>Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                            </div>
                            <div class="grid12-3">
                              <div><img src="<?= $base_url ?>/assets/images/custom-img3.jpg" alt="custom-image"></div>
                              <h4 class="heading">SALE UP TO 30% OFF</h4>
                              <p>Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                            </div>
                            <div class="grid12-3">
                              <div><img src="<?= $base_url ?>/assets/images/custom-img4.jpg" alt="custom-image"></div>
                              <h4 class="heading">SALE UP TO 30% OFF</h4>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <!--nav-->
                </div>
              </nav>
            </div>

          </div>

          <!--row-->

        </div>
      </div>
    </div>
  </div>
</header>