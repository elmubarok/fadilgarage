<?php
  include_once("./config.php");
  
  if(!isset($_SESSION["role"])){
    header("Location: ".$base_url);
  }
?>

<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<?php include("./components/head.php") ?>
<!-- /Head -->

<body>
  <div class="uk-offcanvas-content">

    <?php
    $userid = $_SESSION['userid'];
    
    if($_SESSION['role'] == "admin"){
      $sql = "SELECT * FROM mobil ORDER BY id_mobil DESC";
    }else{
      $sql = "SELECT * FROM mobil WHERE informer = '$userid' ORDER BY id_mobil DESC";
    }

    $result = $con->query($sql);

    if (isset($_GET['delete'])) {
      $id = $_GET['delete'];

      $deleteSQL = $con->query("DELETE FROM mobil WHERE id_mobil = '" . $id . "'");
      $deleteSQL1 = $con->query("DELETE FROM penilaian WHERE id_mobil = '" . $id . "'");

      header("Location: " . $base_url . "/admin.php");
    }
    ?>

    <main>
      <section class="uk-section uk-section-small">
        <div class="uk-container">

          <h3>Hello <?= $_SESSION['role'] == 'admin' ? 'Admin' : 'Penjual'?>.</h3>

          <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

            <!-- User Info -->
            <?php include("./components/user-info.php")?>
            <!-- User Info -->

            <div class="uk-grid-margin uk-first-column uk-width-1-1 uk-width-expand@m">
              <div class="uk-grid-medium" uk-grid>

                <div class="uk-width-expand">
                  <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

                    <div class="uk-first-column">

                      <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                        <div class="uk-grid-collapse uk-child-width-1-1" id="products" uk-grid>

                          <div>
                            <div class="uk-grid-collapse uk-child-width-1-3 stm-products-grid sjs-products-grid tm-products-list js-products-list" uk-grid>

                              <?php while ($row = $result->fetch_assoc()) : ?>
                                <article class="tm-product-card">
                                  <div class="tm-product-card-media">
                                    <div class="tm-ratio tm-ratio-4-3">
                                      <a class="tm-media-box" href="product.html">
                                        <figure class="tm-media-box-wrap"><img src="<?= $base_url ?>/assets/images/cars/2_<?= $row['nopol'] ?>.jpg" alt='' /></figure>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="tm-product-card-body">
                                    <div class="tm-product-card-info">
                                      <div class="uk-text-meta uk-margin-xsmall-bottom"><?= $row['merk'] ?></div>
                                      <h3 class="tm-product-card-title">
                                        <a class="uk-link-heading" href="product.html"><?= $row['seri'] ?> - <?= $row['tahun'] ?></a>
                                      </h3>
                                      <ul class="uk-list uk-text-small tm-product-card-properties">
                                        <li><span class="uk-text-muted">Tipe: </span><span><?= $row['tipe'] ?></span></li>
                                        <li><span class="uk-text-muted">CC: </span><span><?= $row['CC'] ?></span></li>
                                        <li><span class="uk-text-muted">Warna: </span><span><?= $row['warna'] ?></span></li>
                                        <li><span class="uk-text-muted">Kota: </span><span><?= $row['kota'] ?></span></li>
                                        <li><span class="uk-text-muted">Harga: </span><span>Rp <?= str_replace(",", ".", number_format($row['harga'])) ?></span></li>
                                      </ul>
                                    </div>
                                    <div class="tm-product-card-shop" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
                                      <div class="tm-product-card-add">
                                        <button onclick="window.open('<?= $base_url ?>/crud.php?edit=<?= $row['id_mobil'] ?>', '_self')" class="uk-button uk-button-primary uk-margin-small tm-product-card-add-button tm-shine">
                                          ubah
                                        </button>
                                        <button onclick="window.open('<?= $base_url ?>/admin.php?delete=<?= $row['id_mobil'] ?>', '_self')" class="uk-button uk-button-danger tm-product-card-add-button tm-shine">
                                          hapus
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </article>
                              <?php endwhile; ?>

                            </div>
                          </div>

                        </div>
                      </div>

                    </div>

                  </div>
                </div>

              </div>
            </div>

          </div>

        </div>
      </section>

    </main>

    <!-- Footer -->
    <?php include("./components/footer.php") ?>
    <!-- /Footer -->

    <!-- Offcanvas -->
    <?php include("./components/offcanvas.php") ?>
    <!-- /Offcanvas -->

  </div>

  <!-- Script -->
  <?php include("./components/script.php") ?>
  <!-- /Script -->

</body>

</html>