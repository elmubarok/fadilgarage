<?php

  function tPose($arr){

    $datas_x = count($arr[0]);
    $datas_y = count($arr);
    $arr_temp = [];

    for($j=0; $j<$datas_y; $j++){
      
      for($k=0; $k<$datas_x; $k++){

        $arr_temp[$k][$j] = $arr[$j][$k];

      }

    }

    return $arr_temp;

  }

  function prettyEcho1($arr){
    for($k=0; $k<count($arr); $k++){
      echo $arr[$k]."&nbsp;&nbsp;&nbsp;";
    }
    echo "<br><br><br>";
  }

  function prettyEcho2($arr){
    $datas_x = count($arr[0]);
    $datas_y = count($arr);
    $datas_z = count($arr[0][0]);

    for ($i=0; $i < count($arr); $i++) { 
      for($j=0; $j<count($arr[$i]); $j++){
      
        for($k=0; $k<count($arr[$i][$j]); $k++){
          echo $arr[$i][$j][$k]."&nbsp;&nbsp;&nbsp;";
        }
  
        echo "<br>";
  
      }
      echo "<br><br>";
    }

    echo "<br>";
  }

  function prettyEcho3($arr){
    foreach ($arr as $key => $value) {
      echo $value["id"]." => ".$value["val"]." : [".$key."]";
      echo "<br>";
    }
  }

  function prettyEcho($arr){
    $datas_x = count($arr[0]);
    $datas_y = count($arr);

    for($j=0; $j<$datas_y; $j++){
      
      for($k=0; $k<$datas_x; $k++){
        echo $arr[$j][$k]."&nbsp;&nbsp;&nbsp;";
      }

      echo "<br><br>";

    }

    echo "<br>";
  }