<?php
$server = "localhost";
$username = "root";
$password = "";
$database = "fadil_te_umm_db";

// Koneksi dan memilih database di server
// mysql_connect($server,$username,$password);
// mysql_select_db($database);

$con = new mysqli($server, $username, $password, $database);

$brands = ["Honda","Toyota","Daihatsu","Mitsubishi","Lexus",
"Mazda","Chevrolet","BMW","KIA","Nissan","Ford","Marcedes Benz",
"Hyundai","Peugeot","Suzuki","Isuzu"];

$cities = ["Jakarta","Surabaya","Malang","Medan","Banjarmasin",
"Manado","Bandung","Semarang","Denpasar","Jogja","Makasar","Samarinda",
"Palembang",];

$base_url = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
$base_url .= $_SERVER['HTTP_HOST'] . "/new";

session_start();

if(isset($_GET['logout'])){
  session_destroy();
  header("Location: ".$base_url);
}

$terms = [
  ["display" => "Harga", "name" => "t1"],
  ["display" => "Desain", "name" => "t2"],
  ["display" => "CC", "name" => "t3"],
  ["display" => "Bahan Bakar", "name" => "t4"],
  ["display" => "Transmisi", "name" => "t5"]
];

$terms_val = [
  "3" => "Penting",
  "2" => "Ragu - Ragu",
  "1" => "Tidak Penting"
];

$terms_val_2 = [
  "3" => "Cocok",
  "2" => "Ragu - Ragu",
  "1" => "Tidak Cocok"
];